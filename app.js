var user = require('./services/user.js');

App({
    onLaunch: function () {
        var that = this
        user.checkLogin((res) => {
            if (res.code == 0) {
                let info = {
                    avatarUrl: res.data.avatarUrl,
                    nickName: res.data.nickName,
                    registerTime: res.data.registerTime
                }
                wx.setStorageSync('login', true)
                wx.setStorageSync('openid', res.data.openid)
                wx.setStorageSync('uid', res.data.uid)
                that.globalData.userInfo = info
            } else {
                wx.setStorageSync('login', false)
            }
        })
    },

    globalData: {
        userInfo: {
            avatarUrl: '/static/icons/avatar.png',
            nickName: 'Hi，游客'
        },
        token: ''
    },
    debug: true
})