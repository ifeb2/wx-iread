const util = require('../utils/util.js');
const api = require('../config/api.js');

function popularSearches(data, callback) {
    util.request(api.popularSearches, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function searchList(data, callback) {
    util.request(api.searchList, data, 'POST', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    popularSearches,
    searchList,
};