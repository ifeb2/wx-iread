const util = require('../utils/util.js');
const api = require('../config/api.js');


function bookshelf(data, callback) {
    util.request(api.bookshelf, data, 'POST', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function userBookshelfList(data, callback) {
    util.request(api.userBookshelfList, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    bookshelf,
    userBookshelfList,
};