/**
 * 用户相关服务
 */

const util = require('../utils/util.js');
const api = require('../config/api.js');


/**
 * 调用微信登录
 */
function wxLogin(data, callback) {
  util.login((code) => {
    let d = {
      code: code,
      userInfo:JSON.stringify(data.userInfo)
    }
    util.request(api.wxLogin, d, 'POST', (res) => {
      typeof callback == "function" && callback(res)
    })
  })
}

/**
 * 判断用户是否登录
 */
function checkLogin(callback) {
  util.request(api.checkToken, null, 'GET', (res) => {
    typeof callback == "function" && callback(res)
  })
}


module.exports = {
  wxLogin,
  checkLogin,
};