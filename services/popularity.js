const util = require('../utils/util.js');
const api = require('../config/api.js');

function addPopularity(data, callback) {
    util.request(api.popularity, data, 'POST', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function findPopularityList(data, callback) {
    util.request(api.popularityInfo, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function frontPopularityInfo(data, callback) {
    util.request(api.frontPopularityInfo, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    addPopularity,
    findPopularityList,
    frontPopularityInfo,
};