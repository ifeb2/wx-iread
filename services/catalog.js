const util = require('../utils/util.js');
const api = require('../config/api.js');


function findList(data, callback) {
    util.request(api.catalog, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function getContent(data, callback) {
    util.request(api.content, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function readBookByCatalog(data, callback) {
    util.request(api.readBookByCatalog, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    findList,
    getContent,
    readBookByCatalog,
};