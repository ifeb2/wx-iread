const util = require('../utils/util.js');
const api = require('../config/api.js');

function carousel(data, callback) {
    util.request(api.carousel, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}


module.exports = {
    carousel,
};
