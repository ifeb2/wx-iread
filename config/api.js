// var api_url = 'https://www.hdxxs.site/api/';
var api_url = 'http://localhost:8080/api/';
module.exports = {
    wxLogin: api_url + 'wxUser/login', //微信登录
    checkToken: api_url + 'wxUser/checkToken', // 检测超时
    category: api_url + 'book/category', // 查询分类
    book: api_url + 'book', // 书籍列表
    endBookList: api_url + 'book/end/list', // 首页随机10条书籍列表
    bookInfo: api_url + 'book/info', // 书籍详情
    catalog: api_url + 'book/catalog', // 书籍目录
    readBookByCatalog: api_url + 'book/catalog/read', //  阅读记录
    content: api_url + 'book/content', // 书籍目录对应内容
    popularity: api_url + 'book/popularity', // 十秒产生一次人气
    popularityInfo: api_url + 'book/popularity/info', // 人气排名最高的前50
    frontPopularityInfo: api_url + 'book/popularity/front', // 人气排名最高的前50
    createReadRecord: api_url + 'book/record', // 生成用户阅读记录
    userRecordList: api_url + 'book/record/list', // 用户阅读记录 5条
    userRecordMax: api_url + 'book/record/max', // 首页阅读量最大的五条
    bookshelf: api_url + 'book/bookshelf', // 加入书架
    userBookshelfList: api_url + 'book/bookshelf/list', // 加入书架
    popularSearches: api_url + 'book/popular/searches', // 热门推荐 搜索 文字 id
    searchList: api_url + 'book/popular/searches/list', // 热门推荐 搜索 文字 id
    carousel: api_url + 'cms/carousel', // 首页轮播图
};
