// pages/start/start.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: 4,
    getTimeout: '',
    run: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.sumTime()
  },

  /**
   * 定时跳转任务
   */
  sumTime: function() {
    let thta = this
    let timer = setInterval(function() {
      if (thta.data.flag == 0) {
        clearInterval(timer)
        wx.switchTab({
          url: '../qa/qa'
        })
      }
      thta.setData({
        flag: thta.data.flag - 1
      })
    }, 1000);
  },

  toIndex: function() {
    this.setData({
      run: false
    })
    wx.switchTab({
      url: '../qa/qa'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    console.log('页面已隐藏')
    let that = this;
    clearTimeout(that.data.getTimeout);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    let that = this;
    clearTimeout(that.data.getTimeout);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})