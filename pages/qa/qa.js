const util = require('../../utils/util.js');
const user = require('../../services/user.js');
const record = require('../../services/record.js');
const catalog = require('../../services/catalog.js');
const bookshelf = require('../../services/bookshelf.js');
//获取应用实例
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        login_flag: false,
        userInfo: {},
        historyList: [],
        historyShow: false,
        bookshelfList: [],
        no_data: true, //没有数据
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            userInfo: app.globalData.userInfo,
            login_flag: wx.getStorageSync('login')
        })

        if (this.data.login_flag) {
            this.findHistory()
            this.findBookshelf()
        }
    },

    /**
     * 加载用户书架
     */
    findBookshelf: function () {
        let that = this
        let data = {
            userId: wx.getStorageSync('uid')
        }
        bookshelf.userBookshelfList(data, (res) => {
            if (res.code == 0) {
                that.setData({
                    bookshelfList: res.data
                })
            } else {
                that.setData({
                    no_data: false
                })
            }
        })
    },

    /**
     * 获取用户的阅读历史记录 最新的五条
     */
    findHistory: function () {
        let that = this
        let data = {
            userId: wx.getStorageSync('uid')
        }
        record.findUserRecordList(data, (res) => {
            if (res.code == 0) {
                that.setData({
                    historyList: res.data,
                    historyShow: true
                })
            } else {
                that.setData({
                    historyShow: false
                })
            }
        })
    },

    /**
     * 直接阅读
     */
    toRead: function (e) {
        let bookInfoId = e.currentTarget.dataset.id
        let data = {
            bookInfoId: bookInfoId
        }
        catalog.readBookByCatalog(data, (res) => {
            if (res.code == 0) {
                let id = res.data.id
                let current = res.data.catalogIndex
                wx.navigateTo({
                    url: '../content/content?id=' + id + "&current=" + current + "&bookInfoId=" + bookInfoId
                })
            } else {
                wx.showToast({
                    title: '未检索到目录',
                })
            }
        })
    },

    /**
     * 登录绑定
     * @param e
     */
    bindgetuserinfo: function (e) {
        user.wxLogin(e.detail, (res) => {
            if (res.code == 0) {
                wx.setStorageSync('token', res.data.token)
                this.setData({
                    login_flag: true
                });
                wx.setStorageSync('login', true)
                wx.setStorageSync('openid', res.data.userInfo.openid)
                wx.setStorageSync('uid', res.data.userInfo.uid)
                app.globalData.userInfo = res.data.userInfo;
                app.globalData.token = res.data.token;
                this.findHistory()
                this.findBookshelf()
            } else {
                util.showErrorToast('登陆失败')
            }
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */

    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
          userInfo: app.globalData.userInfo,
          login_flag: wx.getStorageSync('login')
        })
        if (this.data.login_flag) {
            this.findHistory()
            this.findBookshelf()
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})