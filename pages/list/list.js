// pages/list/list.js
const popularity = require('../../services/popularity.js');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        type:'',
        pageNum: 1,
        pageSize: 10,
        down: false,
        list: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            type:options.type
        })
        this.initData(options.type)
    },

    /**
     * 加载页面信息
     */
    initData: function (type) {
        if (type == 2) {
            this.findPopularityList()
        }
    },

    /**
     * 人气推荐
     */
    findPopularityList: function () {
        wx.showLoading({
            title: '加载中',
        })

        let that = this
        let data = {
            pageNum: this.data.pageNum,
            pageSize: this.data.pageSize,
        }
        popularity.findPopularityList(data, (res) => {
            if (res.code == 0) {

                let d = that.data.list
                let newRes = d.concat(res.data.data)
                that.setData({
                    list: newRes,
                })

                if (res.data.isHasNextPage){
                    that.setData({
                        down: true,
                        pageNum: that.data.pageNum + 1
                    })
                } else {
                    that.setData({
                        down: false
                    })
                }
            }
            wx.hideLoading()
        })
    },

    loadMore: function () {
        if (this.data.type==2){
            this.findPopularityList()
        }
    },

    /**
     * 跳转到详情页
     */
    toDetails: function (e) {
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '../details/details?id=' + id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})